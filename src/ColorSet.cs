using Microsoft.Xna.Framework;
using StardewModdingAPI;
using System.Collections.Generic;

namespace PrismaticPride
{
	public class ColorSet
	{
		internal protected static IModHelper Helper => ModEntry.Instance.Helper;
		internal protected static IMonitor Monitor => ModEntry.Instance.Monitor;
		internal protected static ModConfig Config => ModConfig.Instance;

		public string key { get; set; }
		public string displayName => Helper.Translation.Get ($"ColorSet.{key}");

		public List<Color> colors { get; set; }
		public int count => colors.Count;

		public Color getColor (int index, float phase)
		{
			int index2 = (index + 1) % count;
			Color result = (Demo.IndexOverride != -1)
				? colors[Demo.IndexOverride]
				: Color.Lerp (colors[index], colors[index2], phase);
			result.A = 255;
			return result;
		}
	}
}
